const formidable = require('formidable')
const http = require('http')
const util = require('util')
const fs = require('fs')
const path = require('path')
const exec = require('child_process').exec

const moviesDir = '/tmp/films'
const tmmCMD = '/home/mburon/dev/buron.coffee/tinyMediaManager/build/tinyMediaManagerCMD.sh'
const tmmExportDir = '/tmp/export'
const tmmtemplateName = 'DarkTemplate'

http.createServer((req, res) => {
  if (req.url === '/upload' &&
      req.method.toLowerCase() === 'post') {
    // parse a file upload
    const form = new formidable.IncomingForm()
    let title

    form.encoding = 'utf-8'
    // set directory for uploads
    form.uploadDir = moviesDir
    // set max file size, 20GB
    form.maxFileSize = 20 * 1024 * 1024 * 1024
    // one file for each upload
    form.multiples = false
    form.keepExtensions = true

    form.on('field', (name, value) => {
      if (name === 'title') {
        title = value
      }
    })

    form.on('fileBegin', (name, file) => {
      if (title && alreadyExists(file, title)) {
        res.statusCode = 400
      }
    })

    form.parse(req, (err, fields, files) => {
      if (err) console.err(err)
      res.writeHead(200, { 'content-type': 'text/plain' })
      res.write('received upload:\n\n')

      const file = files.upload

      if (!alreadyExists(file, title)) {
        renameTheFile(file, title)
      } else {
        return res.end('file ' + createPath(file, title) + ' already exists')
      }

      return updateTMM((err, message) => {
        exportTMM((err, message) => {
          res.end(message)
        })
      })
    })
    return;
  }
  
  // show a file upload form
  res.writeHead(200, {'content-type': 'text/html'});
  res.end(
    '<html>'+
      '<head>'+
          '<meta charset="utf-8">'+
    '</head>'+
    '<form action="/upload" enctype="multipart/form-data" method="post">'+
      '<input type="text" name="title"><br>'+
      '<input type="file" name="upload"><br>'+
      '<input type="submit" value="Upload">'+
      '</form>' +
    '</html>'

  );
}).listen(8200);

function renameTheFile (file, title) {
  const newPath = createPath(file, title)
  fs.rename(file.path, newPath, err => {
    if (err) console.log(err)
  })
}

function alreadyExists (file, title) {
  return fs.existsSync(createPath(file, title))
}

function createPath (file, title) {
  const parsedPath = path.parse(file.path)
  const newPath = path.format({
    dir: parsedPath.dir,
    name: title,
    ext: parsedPath.ext
  })

  return newPath
}


function updateTMM (next) {
  return exec(tmmCMD + ' -update -scrapeNew -rename', (error, stdout, stderr) => {
    console.log(stderr)
    if (next) {
      next(error, stdout)
    } else {
      console.error(error)
    }
  })
}

function exportTMM (next) {
  return exec(tmmCMD + ' -export ' + tmmtemplateName + ' ' + tmmExportDir, (error, stdout, stderr) => {
    console.log(stderr)
    if (next) {
      next(error, stdout)
    } else {
      console.error(error)
    }
  })
}
